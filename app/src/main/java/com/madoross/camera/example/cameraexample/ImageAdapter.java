package com.madoross.camera.example.cameraexample;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.media.Image;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by bangseungjin on 2018. 1. 4..
 */

public class ImageAdapter extends BaseAdapter {

    String[] getImgList;
    private Context mContext;

    ImageAdapter(Context c){
        mContext = c;
        getImgList = getImageList();

    }

    private String[] getImageList(){
        String path = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + File.separator + "croptest" + File.separator;

        File list = new File(path);

        String[] imgList = list.list(new FilenameFilter() {
            @Override
            public boolean accept(File file, String filename) {
                return true;
            }
        });

        Log.d("imgList", " : " + imgList);

        return imgList;
    }

    @Override
    public int getCount() {

        return (0 != getImgList.length) ? getImgList.length : 0 ;
    }

    @Override
    public Object getItem(int position) {

        return (null != getImgList[position]) ? getImgList[position] : null;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View myView;

        LayoutInflater li = ((Activity) mContext).getLayoutInflater();
        myView = li.inflate(R.layout.gallery_item, null);

        ImageView imageView = (ImageView) myView.findViewById(R.id.imageView);
        TextView textView = (TextView) myView.findViewById(R.id.textView);



        if(getImgList.length != 0){


            if(convertView == null){
                myView.setLayoutParams(new GridView.LayoutParams(350, 350));
                myView.setPadding(1, 1, 1, 1);

            } else {
                myView = (View) convertView;
            }

            String path = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + File.separator + "croptest" + File.separator + getImgList[position];

            Bitmap bitmap = BitmapFactory.decodeFile(path);

            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            Glide.with(mContext).load(path).into(imageView);

            try {
                // 사진 정보 읽어들이기
                ExifInterface exif = new ExifInterface(path);
                String getExif = exif.getAttribute(ExifInterface.TAG_IMAGE_DESCRIPTION);
                Log.d("exit path", " : " + path);
                Log.d("exif", " : " + getExif);

                if(getExif != null){
                    textView.setText("업로드 완료");
                } else {
                    textView.setText("업로드 대기");
                }

            } catch(IOException e){
                e.printStackTrace();
            }




            return myView;
        } else {
            return null;
        }

    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }
}
